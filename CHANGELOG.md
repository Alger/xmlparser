# [ v0.0.2 ]

## Feature

- 增加XmlDocumentManager用于管理文档信息

- 初始化时可能为None的属性保留Option定义，减少内存占用

- 标签可以直接通过id获取

## Bugfix

无

## Remove

无

# [ v0.0.1 ]

## Feature

- 可以正常解析xml文件

## Bugfix

- 修复缓存长度大于标签长度时出现的数组越界异常
- 减少不必要的循环，提高性能

## Remove

无
