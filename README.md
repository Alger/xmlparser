<div align="center">
<h1>xmlparser</h1>
</div>

<p align="center">
  <img alt="" src="https://img.shields.io/badge/apache-2.0-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/release-v0.0.1-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/build-pass-brightgreen" style="display: inline-block;" />
<img alt="" src="https://img.shields.io/badge/cjc-v0.58.3-brightgreen" style="display: inline-block;" />
  <img alt="" src="https://img.shields.io/badge/cjov-100%25-brightgreen" style="display: inline-block;" />
</p>

## 介绍

### 项目特性

- 流式读取分析，标签匹配模式使用 byte 数组，单字节匹配直接使用字节码匹配

- 使用了固定缓冲区提高性能，默认读取缓存大小为 2048 字节，可指定缓冲区大小，不建议使用过大或者过小的缓冲区（特殊场景除外）

- 文件编码必须为 utf-8（仓颉 String 编码目前仅支持 utf-8），虽然解析器不再使用 String，但若为其他格式的编码，可能会在获取字符串内容时出错

### 项目计划

- 支持自定义注释标签名如"<!Doctype>"

- 报错提示显示所在行号

## 项目架构

### 源码目录

```shell
.
├── CHANGELOG.md                      # 版本更新日志
├── READEME.OpenSource                # 开源说明
├── LICENSE                           # 开源协议
├── README.md                         # 整体介绍
└── src                               # 源码目录
    ├── main.cj                       # 主入口文件
    ├── test                          # 单元测试目录
    │   └── XmlDocumentParserTest.cj  # xml文档解析器单元测试
    ├── context                       # 文档解析上下文目录
    │   └── XmlDocumentParser.cj      # xml文档解析器
    │   └── XmlDocumentManager.cj     # xml文档信息管理类
    └── structs                       # xml文档解析使用到的结构体存放目录
        ├── XmlDocument.cj            # Xml文档信息结构体，也是提取xml文档内容主要入口
        ├── XmlElment.cj              # Xml节点信息结构体，存储Xml节点内容
        ├── XmlElmentTags.cj          # Xml标签结构体，内容为xml常用标签结构，如"<!--"
        └── XmlParserConfig.cj        # Xml解析器配置结构体
```

## 使用说明

### 编译构建

纯仓颉项目，直接使用 cjpm 进行构建即可

```shell
cjpm update
cjpm build
```

### 功能示例

#### xml 文档解析示例

```shell
main() {
    let file = File("F:/GitHub/Chat/pom.xml",OpenMode.Read)
    let config = XmlParserConfig(2048,true)
    # let parser = XmlDocumentPaser(file,config)
    # let document = parser.read()
    # let doc = document.getOrThrow()
    # println(doc)

    # 两种方式二选一即可，manager类提供了更多的针对文档信息的方法，如getElementById

    let docManager = XmlDocumentManager(config)
    docManager.parseDocument(file)
    let document = docManager.getDocument();
    println(document.getOrThrow())
}
```

#### 执行结果如下

```shell
<?xml version="1.0" encoding="UTF-8" ?>
 <!-- test -->
 <project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
<modelVersion>4.0.0</modelVersion>
<groupId>org.example</groupId>
<artifactId>Chat</artifactId>
<version>1.0-SNAPSHOT</version>
<properties>
<maven.compiler.source>22</maven.compiler.source>
<maven.compiler.target>22</maven.compiler.target>
<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
</properties>
</project>
```

## 参与贡献

欢迎提交 PR、提 issue 以及参与任何形式的贡献。
